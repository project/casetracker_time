<?php

function casetracker_time_estimate_admin_settings() {
  $form = array();

  // load the casetracker statuses to use in the workflow configuration
  if (function_exists('casetracker_case_state_load')) {
    $ct_status_list = casetracker_case_state_load(NULL, 'status');
  }

  $form['casetracker_time_reqestimate_assigned_comment'] = array(
    '#title' => t('Require estimation to assigned'),
    '#type' => 'checkbox',
    '#required' => false,
    '#description' => t('Require estimation to the assigned user before to start commenting.'),
    '#default_value' => variable_get('casetracker_time_reqestimate_assigned_comment', ''),
  );

  $form['casetracker_time_reqestimate_timing'] = array(
    '#title' => t('Require estimation to start case timing.'),
    '#type' => 'checkbox',
    '#required' => false,
    '#description' => t('An estimation for the case must exist before start a timing.'),
    '#default_value' => variable_get('casetracker_time_reqestimate_timing', ''),
  );

  $form['casetracker_time_reqestimate_assigned_start'] = array(
    '#title' => t('Require estimation at change status'),
    '#type' => 'select',
    '#multiple' => true,
    '#required' => false,
    '#options' => $ct_status_list,
    '#description' => t('Require estimation at change the status case to selected satuses to the assigned user.'),
    '#default_value' => variable_get('casetracker_time_reqestimate_assigned_start', ''),
  );

  $form['casetracker_time_denyall_comment_estimate'] = array(
    '#title' => t('Deny all comment if not estimated'),
    '#type' => 'checkbox',
    '#required' => false,
    '#description' => t('Deny comments to all users if the case is not estimated.'),
    '#default_value' => variable_get('casetracker_time_denyall_comment_estimate', ''),
  );

  return system_settings_form($form);
}

function casetracker_time_timing_admin_settings() {
  $form = array();
  $node_types = node_get_types();
  $ct_status_list = casetracker_case_state_load(NULL, 'status');
  $ct_status_list = array(0 => 'None') + $ct_status_list;

  $form['time_track_validations_ct_status_stop'] = array(
    '#title' => t('Status stop'),
    '#type' => 'select',
    '#options' => $ct_status_list,
    '#description' => t('Status to set to Node case when the time track stops.'),
    '#default_value' => variable_get('time_track_validations_ct_status_stop', ''),
  );

  $form['casetracker_time_status_start_id'] = array(
    '#title' => t('Change case status on timer start event'),
    '#type' => 'select',
    '#options' => $ct_status_list,
    '#description' => t('Set the case status to the selected value when a timing start.'),
    '#default_value' => variable_get('casetracker_time_status_start_id', ''),
  );

  $form['casetracker_time_status_stop_id'] = array(
    '#title' => t('Change case status on timer stop event'),
    '#type' => 'select',
    '#options' => $ct_status_list,
    '#description' => t('Set the case status to the selected value when a timing stop.'),
    '#default_value' => variable_get('casetracker_time_status_stop_id', ''),
  );

  $form['time_track_validations_ct_status_cancel'] = array(
    '#title' => t('Status cancel'),
    '#type' => 'select',
    '#options' => $ct_status_list,
    '#description' => t('Status cancel to skip the estimation validation of status change.'),
    '#default_value' => variable_get('time_track_validations_ct_status_cancel', ''),
  );

  $form['casetracker_time_unique_active_timing'] = array(
    '#type' => 'checkbox',
    '#title' => t('Pause current started cases'),
    '#required' => false,
    '#description' => t('Pause all the started cases if only one case must be in working process.'),
    '#default_value' => variable_get('casetracker_time_unique_active_timing', ''),
  );

  $form['casetracker_time_reqtimer_status'] = array(
    '#title' => t('Require timing on status change'),
    '#type' => 'select',
    '#multiple' => true,
    '#required' => false,
    '#options' => $ct_status_list,
    '#description' => t('Validate that some timings was created when the case change to the selected status.'),
    '#default_value' => variable_get('casetracker_time_reqtimer_status', ''),
  );

  $form['casetracker_time_autostop_status'] = array(
    '#title' => t('Autostop the timings on status change'),
    '#type' => 'select',
    '#multiple' => true,
    '#required' => false,
    '#options' => $ct_status_list,
    '#description' => t('Pause the active case timings when the case change to the selected status.'),
    '#default_value' => variable_get('casetracker_time_autostop_status', ''),
  );

  //TODO: include timing summary in block will be configurable
  return system_settings_form($form);
}
