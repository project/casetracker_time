<?php
class views_handler_field_time_tracker_form extends views_handler_field {
 function construct() {
   parent::construct();
   $this->additional_fields['uid'] = 'uid';
   $this->additional_fields['nid'] = 'nid';
 }

 function render($values) {
   $uid = $values->{$this->aliases['uid']};
   $nid = $values->{$this->aliases['nid']};
   return time_track_tracked_time($nid, $uid, 'form');
 }
}
