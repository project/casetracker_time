<?php

/**
 * Implementation of hook_views_data()
 */
function casetracker_time_views_data() {

  // CASETRACKER TIME

  $data['casetracker_time']['table']['group']  = t('Casetracker time');

  $data['casetracker_time']['table']['base'] = array(
    'field' => 'cttid',
    'title' => t('Case Timing'),
    'help' => t("Case timings."),
  );

  $data['casetracker_time']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
    'users' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    ),
  );

  $data['casetracker_time']['cttid'] = array(
    'title' => t('ID'),
    'help' => t('The case time ID'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
  );

  $data['casetracker_time']['uid'] = array(
    'title' => t('User'),
    'help' => t('The user who timed the node'),
    'filter' => array(
      'handler' => 'casetracker_views_handler_filter_user_options',
      'help' => t('Filter timers by the user.'),
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_user_uid',
      'help' => t('Filter timers by the user.'),
    ),
    'relationship' => array(
      'base' => 'users',
      'field' => 'uid',
      'handler' => 'views_handler_relationship',
      'label' => t('Users'),
     ),
  );

  $data['casetracker_time']['nid'] = array(
    'title' => t('Node'),
    'help' => t('The node who was timed.'),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Node'),
    ),
  );
 
  $data['casetracker_time']['seconds'] = array(
    'title' => t('Total timed time represented in seconds'),
    'help' => t('The total amount of time in seconds that was timed for the node.'),
    'field' => array(
      'handler' => 'views_handler_field_tracked_time',
      'click sortable' => TRUE,
      'sort' => array(
        'handler' => 'views_handler_sort_numeric',
      ),
    ),
  );

  $data['time_track']['secondsraw'] = array(
    'real field' => 'seconds',
    'title' => t('Total timed time represented in seconds raw'),
    'help' => t('The total amount of time in seconds raw that was timed for the node.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  $data['casetracker_time']['hours'] = array(
    'title' => t('Total timed time represented in hours'),
    'help' => t('The total amount of time in hours that was timed for the node.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
      'sort' => array(
        'handler' => 'views_handler_sort_numeric',
      ),
    ),
  );

  $data['casetracker_time']['estimation'] = array(
    'title' => t('Estimated time represented in hours'),
    'help' => t('The estimated time in hours to complete the case.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
      'sort' => array(
        'handler' => 'views_handler_sort_numeric',
      ),
    ),
  );

  // CASETRACKER TIMELOG

  $data['casetracker_timelog']['table']['group']  = t('Casetracker timelog');

  $data['casetracker_timelog']['table']['base'] = array(
    'field' => 'cttlid',
    'title' => t('Casetracker timelogs'),
    'help' => t("Case timelogs."),
  );

  $data['casetracker_timelog']['table']['join'] = array(
    'casetracker_time' => array(
      'left_field' => 'cttid',
      'field' => 'cttid',
    ),
  );

  $data['casetracker_timelog']['cttlid'] = array(
    'title' => t('ID'),
    'help' => t('The timelog ID'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
  );

  $data['casetracker_timelog']['cttid'] = array(
    'title' => t('Case time log ID'),
    'help' => t('The time ID the log is related to.'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'casetracker_time',
      'field' => 'cttid',
      'label' => t('Time logs.'),
    ),
  );

  $data['casetracker_timelog']['start'] = array(
    'title' => t('Start'),
    'help' => t('Time log starting time'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['casetracker_timelog']['stop'] = array(
    'title' => t('Stop'),
    'help' => t('Time log stopping time'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['casetracker_timelog']['hours'] = array(
    'title' => t('Time log hours'),
    'help' => t('Time log hours of duration for this interval'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  return $data;
}

function casetracker_time_views_data_alter(&$data) {
  $data['node']['total_tracked'] = array(
    'title' => t('Total tracked time'),
    'help' => t('The total amount of time tracked for that node.'),
    'sort' => array(
      'handler' => 'views_handler_sort_total_tracked',
    ),
    'field' => array(
      'handler' => 'views_handler_field_total_tracked_time',
      'click sortable' => TRUE,
      'sort' => array(
        'handler' => 'views_handler_sort_numeric',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_handlers()
 */
function casetracker_time_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'casetracker_time') . '/includes',
    ),
    'handlers' => array(
      'views_handler_field_casetracker_time_total' => array(
        'parent' => 'views_handler_field_numeric',
      ),
      'views_handler_field_castracker_time' => array(
        'parent' => 'views_handler_field',
      ),
      'views_handler_field_casetracker_time_form' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}

/**
 * Implementation of hook_date_api_fields().
 */
function casetracker_time_date_api_fields($field) {
  $values = array(
    // The type of date: DATE_UNIX, DATE_ISO, DATE_DATETIME.
    'sql_type' => DATE_UNIX,
    // Timezone handling options: 'none', 'site', 'date', 'utc'.
    'tz_handling' => 'site',
    // Needed only for dates that use 'date' tz_handling.
    'timezone_field' => '',
    // Needed only for dates that use 'date' tz_handling.
    'offset_field' => '',
    // Array of "table.field" values for related fields that should be
    // loaded automatically in the Views SQL.
    'related_fields' => array(),
    // Granularity of this date field's db data.
    'granularity' => array('year', 'month', 'day', 'hour', 'minute', 'second'),
  );
  switch ($field) {
    case 'casetracker_time.created':
    case 'casetracker_time.estimated':
    case 'casetracker_timelog.start':
    case 'casetracker_timelog.stop':
      return $values;
  }
}

/**
 * Implement hook_date_api_tables().
 */
function casetracker_time_date_api_tables() {
  return array('casetracker_time', 'casetracker_timelog');
}

