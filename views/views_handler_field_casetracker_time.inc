<?php
class views_handler_field_tracked_time extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['uid'] = 'uid';
    $this->additional_fields['nid'] = 'nid';
  }
  
  function option_definition() {
    $options = parent::option_definition();
    $options['tracker_form'] = array('default' => FALSE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['tracker_form'] = array(
      '#title' => t('Display as a form'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['tracker_form']),
    );
  }
  
  function render($values) {
    $uid = $values->{$this->aliases['uid']};
    $nid = $values->{$this->aliases['nid']};
    if (!empty($this->options['tracker_form'])) {
      return time_track_tracked_time($nid, $uid, 'form');
    }
    else {
      return theme('time_track_time', time_track_prepare_time(time_track_get_time($nid, $uid)));
    }
  }
}
