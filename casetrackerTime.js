Drupal.casetrackerTime = Drupal.casetrackerTime || {};
Drupal.casetrackerTime.execution = Drupal.casetrackerTime.execution || {};

Drupal.behaviors.dynamicTimeTracker = function (context) {
 $('form.node-timing', context).each(function () {
    Drupal.casetrackerTime.execution = new Drupal.casetrackerTime(this);
  });
};

/**
 * A time timer switch object
 */
Drupal.casetrackerTime = function (form) {
  var timer = this;
  this.form = form;
  this.nid = $('#'+ form.id +' input[name="nid"]').attr('value');
  this.uid = $('#'+ form.id +' input[name="uid"]').attr('value');
  this.button = $('#'+ form.id +' input[type="submit"]');
  this.url = Drupal.settings.casetracker_time.url + '/' + this.nid + '/' + this.uid;
  this.counter = $('#'+ form.id +' .counter');
  this.context = 'execution';
  this.status  = 'stopped';
  this.select_status = $('select[name*="case_status_id"]');
  this.div_status = $('div.status');
  this.stop_status_id = Drupal.settings.casetracker_time.stop_status_id;
  this.start_status_id = Drupal.settings.casetracker_time.start_status_id;
  this.status_list = Drupal.settings.casetracker_time.status_list;
  this.initialize();
  setInterval (function() { timer.initialize(); }, 30000);
};

/**
 * Initialize the time timer
 */
Drupal.casetrackerTime.prototype.initialize = function () {
  var timer = this;

  $.ajax({
    type: "GET",
    url: timer.url,
    dataType: 'json',
    success: function (status) {
      $(timer.counter).countdown({compact:true});
      $(timer.counter).countdown('change', 'since', -status['time']).countdown('resume');
      timer.status = status['status'];
      timer.getStatus();
    },
    error: function (xmlhttp) {
      alert(Drupal.ahahError(xmlhttp, timer.startURL));
    }
  });
};

Drupal.casetrackerTime.prototype.getStatus = function() {
  var timer = this;
  if (timer.status == 'ongoing') {
    $(timer.button).unbind().bind('click', function(event) {
        timer.stopTracking();
        event.preventDefault();
    });
    $(timer.counter).countdown('resume');
    $(timer.button).val(Drupal.t('Stop'));
    timer.initStatus(timer.start_status_id); 
    $(timer.form).removeClass('node-timer-start').addClass('node-timer-stop');
  }
  else {
    $(timer.button).unbind().bind('click', function(event) {
        timer.startTracking();
        event.preventDefault();
    });
    $(timer.counter).countdown('pause');
    $(timer.button).val(Drupal.t('Start'));
    timer.initStatus(timer.stop_status_id); 
    $(timer.form).removeClass('node-timer-stop').addClass('node-timer-start');
  }
}


/**
 * Starts timing
 */
Drupal.casetrackerTime.prototype.startTracking = function () {
  var timer = this;

  timer.status = 'ongoing';

  // Ajax GET request for starting time timer
  $.ajax({
    type: "GET",
    url: timer.url + '/start',
    dataType: 'json',
    success: function (status) {
      $(timer.counter).countdown('change', {since: -status['time']}).countdown('resume');
      $(timer.button).val(Drupal.t('Stop'));
      $(timer.form).removeClass('node-timer-start').addClass('node-timer-stop');
      timer.getStatus();
    },
    error: function (xmlhttp) {
      alert(Drupal.ahahError(xmlhttp, timer.startURL));
    }
  });
};

/**
 * Stops timing
 */
Drupal.casetrackerTime.prototype.stopTracking = function () {
  var timer = this;

  timer.status = 'stop';

  $.ajax({
    type: "GET",
    url: timer.url + '/stop',
    dataType: 'json',
    success: function (status) {
      $(timer.counter).countdown('change', {since: -status['time']}).countdown('pause');
      $(timer.button).val(Drupal.t('Start'));
      $(timer.form).removeClass('node-timer-stop').addClass('node-timer-start');
      timer.getStatus();
    },
    error: function (xmlhttp) {
      alert(Drupal.ahahError(xmlhttp, timer.startURL));
    }
  });
};

/**
 * Initialize case status
 */
Drupal.casetrackerTime.prototype.initStatus = function(status_id) {
  var timer = this;
  $(timer.select_status).val(status_id);
  $(timer.div_status).html(timer.status_list[status_id]);
};

