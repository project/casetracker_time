Description
-----------

The goal of this module is help the companies that use Case Tracker module to
organize the development tasks giving more vision in how the time is spend.
At the beginning of development of the module we pretended a simple time tracking
to make possible to meter the time dedicated to each case, but in a company as
Ideup! (where I started developing this module) and when we manage at least six
simultaneously projects we discover that are needed some rules to make really
useful this task. Of course, nothing interesting we get from only store timings
if the company doesn't have the information in easy format that we can analyse
and get understanding.

After install and test many time trackers I found many interesting concepts
but nothing that reach what we are expecting. There are so many modules but
unfortunately some of them are unmaintained and doesn't provide a hooks to make
possible the extension of the features without hack the contrib module. Learning
for the mistakes of that modules I try to improve the approach with a main goals
that I will try to reach step by step:

- Make an extensible module providing hooks to react to the main events of the 
timers (stop and start)                                                        
- Give a configurable set of rules to adapt the use of the timers better to    
each corporation philosophy. More or less strict (depends on configuration).   
- Estimations and timings are dependent, if you want to meter the time probably
is because you want to make better forecasts of features deliveries. We think  
in both as dependent                                                           
- Total integration with Views with custom handlers providing a variety of     
reports to analyse easily the spend time                                       
- One click interface, we believe which the successful in the use of task      
timing is to make easy to timing. Then we provide a widget block to start and  
stop a timing. Easy as one click                                               
- Introduce an interesting concept of timing context. All the tasks have       
many contexts until to finish and many participants that interact in many      
task contexts. We add to each timing one context: execution, write, read,      
think, deploy, review, meeting, etc. Is possible to detect each context if the 
development process and the case states are well defined. See (time-contexts)  

Inspiration
-----------


Configuration
-------------

admin/ctt/workflow - Timing workflow configuration, you can set the status     .
rules of the case depending in some general conditions. Also you can select a  .
status to change the case in the events of start/stop of the timing            .
admin/ctt/estimations - Estimations workflow configuration                     .

Features

- Case tracker timing, store timings for the case nodes                        .
- Case tracker estimations, store estimations of the cases for each person that.
participate in the task                                                        .
- Views integration, create your own Case tracker timing and estimations report.

Time contexts
-------------

Requirements
-------------

Drupal 6.x
Views
Case tracker
Library

Support
-------

Please use the issue queue to report bugs with this module at
http://drupal.org/project/issues/casetracker_time

API
---

